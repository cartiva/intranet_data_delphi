unit scoTodayDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure swMetricDataToday;
    procedure GLPDTIM;
    procedure GLPTRNS;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'scoToday';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\scotest\sco.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;


procedure TDM.swMetricDataToday;
var
  proc: string;
begin
  proc := 'swMetricDataToday';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure swMetricDataToday()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('swMetricDataToday.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
  end;
end;


procedure TDM.GLPDTIM;
var
  proc: string;
begin
  proc := 'GLPDTIM';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpGLPDTIM'')');
      CloseQuery(adsQuery);
      PrepareQuery(AdsQuery,'insert into tmpGLPDTIM ' +
          '(gqco#, gqtrn#, gquser, gqdate, gqtime, gqfunc) values ' +
          '(:GQCO, :GQTRN, :GQUSER, :GQDATE, :GQTIME, :GQFUNC)');
      OpenQuery(AdoQuery, 'select gqco#,gqtrn#,gquser,gqdate,gqtime,gqfunc from ' +
          'rydedata.glpdtim where ' +
          'gqdate = year(curdate())*10000 + month(curdate())*100 + day(curdate()) ' +
          'and gqtime <> 0' + ' and gqtrn# <> 0');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('GQCO').AsString := AdoQuery.FieldByName('GQCO#').AsString;
        AdsQuery.ParamByName('GQTRN').AsInteger := AdoQuery.FieldByName('GQTRN#').AsInteger;
        AdsQuery.ParamByName('GQUSER').AsString := AdoQuery.FieldByName('GQUSER').AsString;
        AdsQuery.ParamByName('GQDATE').AsInteger := AdoQuery.FieldByName('GQDATE').AsInteger;
        AdsQuery.ParamByName('GQTIME').AsInteger := AdoQuery.FieldByName('GQTIME').AsInteger;
        AdsQuery.ParamByName('GQFUNC').AsString := AdoQuery.FieldByName('GQFUNC').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE tmpGLPTIMaddTS()'); // populates the gqTS field
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpGLPDTIM'')');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('glpdtim.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.GLPTRNS;
var
  proc: string;
begin
  proc := 'GLPTRNS';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpGLPTRNS'')');
      CloseQuery(adsQuery);
      PrepareQuery(AdsQuery, 'insert into tmpGLPTRNS values' +
          '(:GTCO, :GTTRN, :GTSEQ, :GTDTYP, :GTTYPE, :GTPOST, :GTRSTS,' +
          ':GTADJUST, :GTPSEL, :GTJRNL, :GTDATE, :GTRDATE, :GTSDATE,' +
          ':GTACCT, :GTCTL, :GTDOC, :GTRDOC, :GTRDTYP, :GTODOC,' +
          ':GTREF, :GTVND, :GTDESC, :GTTAMT, :GTCOST, :GTCTLO, :GTOCO)');
      OpenQuery(AdoQuery, 'select * from rydedata.glptrns where gtpost = ''Y''' +
          ' and gttrn# in (select gqtrn# from rydedata.glpdtim where gqtrn# <> 0' +
          ' and gqtime <> 0 and gqdate = year(curdate())*10000 + month(curdate())*100 + day(curdate()))');
      while not AdoQuery.eof do
      begin
        AdsQuery.ParamByName('GTCO').AsString := AdoQuery.FieldByName('GTCO#').AsString;
        AdsQuery.ParamByName('GTTRN').AsInteger := AdoQuery.FieldByName('GTTRN#').AsInteger;
        AdsQuery.ParamByName('GTSEQ').AsInteger := AdoQuery.FieldByName('GTSEQ#').AsInteger;
        AdsQuery.ParamByName('GTDTYP').AsString := AdoQuery.FieldByName('GTDTYP').AsString;
        AdsQuery.ParamByName('GTTYPE').AsString := AdoQuery.FieldByName('GTTYPE').AsString;
        AdsQuery.ParamByName('GTPOST').AsString := AdoQuery.FieldByName('GTPOST').AsString;
        AdsQuery.ParamByName('GTRSTS').AsString := AdoQuery.FieldByName('GTRSTS').AsString;
        AdsQuery.ParamByName('GTADJUST').AsString := AdoQuery.FieldByName('GTADJUST').AsString;
        AdsQuery.ParamByName('GTPSEL').AsString := AdoQuery.FieldByName('GTPSEL').AsString;
        AdsQuery.ParamByName('GTJRNL').AsString := AdoQuery.FieldByName('GTJRNL').AsString;
        AdsQuery.ParamByName('GTDATE').AsDateTime := AdoQuery.FieldByName('GTDATE').AsDateTime;
        AdsQuery.ParamByName('GTRDATE').AsDateTime := AdoQuery.FieldByName('GTRDATE').AsDateTime;
        AdsQuery.ParamByName('GTSDATE').AsDateTime := AdoQuery.FieldByName('GTSDATE').AsDateTime;
        AdsQuery.ParamByName('GTACCT').AsString := AdoQuery.FieldByName('GTACCT').AsString;
        AdsQuery.ParamByName('GTCTL').AsString := AdoQuery.FieldByName('GTCTL#').AsString;
        AdsQuery.ParamByName('GTDOC').AsString := AdoQuery.FieldByName('GTDOC#').AsString;
        AdsQuery.ParamByName('GTRDOC').AsString := AdoQuery.FieldByName('GTRDOC#').AsString;
        AdsQuery.ParamByName('GTRDTYP').AsString := AdoQuery.FieldByName('GTRDTYP').AsString;
        AdsQuery.ParamByName('GTODOC').AsString := AdoQuery.FieldByName('GTODOC#').AsString;
        AdsQuery.ParamByName('GTREF').AsString := AdoQuery.FieldByName('GTREF#').AsString;
        AdsQuery.ParamByName('GTVND').AsString := AdoQuery.FieldByName('GTVND#').AsString;
        AdsQuery.ParamByName('GTDESC').AsString := AdoQuery.FieldByName('GTDESC').AsString;
        AdsQuery.ParamByName('GTTAMT').AsCurrency := AdoQuery.FieldByName('GTTAMT').AsCurrency;
        AdsQuery.ParamByName('GTCOST').AsCurrency := AdoQuery.FieldByName('GTCOST').AsCurrency;
        AdsQuery.ParamByName('GTCTLO').AsString := AdoQuery.FieldByName('GTCTLO').AsString;
        AdsQuery.ParamByName('GTOCO').AsString := AdoQuery.FieldByName('GTOCO#').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpGLPTRNS'')');
//      ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE StgArkGLPTRNS()');
//      curLoadPrevLoad('GLPTRNS');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('glpdtim.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


//************************Utilities*******************************************//

function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;


end.


