program pto_payout;

{$APPTYPE CONSOLE}
{ TODO :
2/16/15: 0 elgible employees email failed silently, foster email was missing period before com,
no email sent, no failure notification
add sorum to admin email list}
uses
  SysUtils,
  StrUtils,
  pto_payoutDM in 'pto_payoutDM.pas' {DM: TDataModule};

begin

  try
    try
      DM := TDM.Create(nil);
      DM.populatePtoPayoutEligible;
      if eligibleEmployees = 0 then
{ DONE : Add Ben Foster to no eligible email }
        DM.SendMail('jandrews@cartiva.com,bfoster@rydellcarscom','no payout eligible employees this week','')
      else
      begin
        dm.employeeEmails;
        dm.authEmails;
        dm.auth_authEmails;
        dm.adminEmails;
      end;
    except
      on E: Exception do
      begin
        DM.SendMail('jandrews@cartiva.com', 'pto_payout failed', E.Message);
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
