unit ucinvDM;

interface

uses
  SysUtils, Classes,
  IdAllFTPListParsers, adsdata, adsfunc, adstable, adscnnct, DB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SendMail(Subject: string; Body: string='');
    procedure ucinv_update;
  end;

var
  DM: TDM;
  AdsCon: TADSConnection;
  AdsQuery: TAdsQuery;
  eMessage: string = '';
implementation

{$R *.dfm}

{ TDM }

constructor TDM.Create(AOwner: TComponent);
begin
  AdsCon := TADSConnection.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdsQuery.AdsConnection := AdsCon;
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\scotest\sco.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
end;

procedure TDM.ucinv_update;
begin
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'EXECUTE PROCEDURE ucinv_update_daily()';
    AdsQuery.ExecSQL;
  except
    on E: Exception do
    begin
      eMessage := eMessage + ' ucinv_update_daily failed: ' + E.Message;
      exit;
    end;
  end;
end;

destructor TDM.Destroy;
begin
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  inherited;
end;

procedure TDM.SendMail(Subject, Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
//        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
//            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
//            '''' + 'send mail failed' + '''' + ', ' +
//            '''' + 'no sql - line 427' + '''' + ', ' +
//            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


end.
