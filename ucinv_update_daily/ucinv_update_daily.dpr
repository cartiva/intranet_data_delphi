program ucinv_update_daily;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  StrUtils,
  ucinvDM in 'ucinvDM.pas' {DM: TDataModule};

begin

  try
    try
      DM := TDM.Create(nil);
      DM.ucinv_update;
    except
      on E: Exception do
      begin
        DM.SendMail('ucinv_update_daily failed', E.Message);
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
