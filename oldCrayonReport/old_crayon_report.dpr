program old_crayon_report;

{$APPTYPE CONSOLE}
(*)
replaces the daily call to ArkonaGLDownloader on ads.cariva.com
allows the tool to continue to generate crayon reports as we work on 
transitioning that function to Vision
(**)
uses
  SysUtils,
  StrUtils,
  ocrDM in 'ocrDM.pas' {DM: TDataModule};

begin

  try
    try
      DM := TDM.Create(nil);
      DM.glDetailsTable;
      if glDetailsTablePassed then DM.updateVehicleCostTable;
      if updateVehicleCostTablePassed then DM.createGrossLogTable;
      if not (glDetailsTablePassed and updateVehicleCostTablePassed
        and createGrossLogTablePassed) then DM.SendMail('old_crayon_report failed: ', eMessage);
    except
      on E: Exception do
      begin
        DM.SendMail('old_crayon_report failed', E.Message);
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
